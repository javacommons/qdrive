﻿#include "widget.h"
#include "ui_widget.h"

#include <QtCore>
#include <QtGui>
#include <QtWidgets>

#include <QtNetwork>
#include <QtSql>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    m_read_qiita = env.value("READ_QIITA");
    if(m_read_qiita.isEmpty())
    {
        QMessageBox::information(this, "確認", "環境変数READ_QIITAが設定されていません");
        qApp->exit(1);
    }
    qDebug() << "m_read_qiita=" << m_read_qiita;
}

Widget::~Widget()
{
    delete ui;
}

QByteArray Widget::GetFromUrl(QUrl url, QMap<QString, QString> &map)
{
    QNetworkRequest request(url);
    request.setRawHeader("Authorization", QString("Bearer %1").arg(m_read_qiita).toLatin1());

    QNetworkAccessManager nam;
    QNetworkReply *reply = nam.get(request);
    QEventLoop loop;
    while(!reply->isFinished())
    {
        loop.processEvents(QEventLoop::AllEvents);
    }
    QList<QByteArray> names = reply->rawHeaderList();
    //qDebug() << reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
    for(int i=0; i<names.size(); i++)
    {
        map[QString::fromLatin1(names[i])] = QString::fromLatin1(reply->rawHeader(names[i]));
    }
    map["HttpStatus"] = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toString();
    QByteArray result = reply->readAll();
    reply->deleteLater();
    return result;
}

void Widget::on_btnTest1_clicked()
{
    qDebug() << "test1です";

    QUrl url("https://qiita.com/api/v2/items");
    QUrlQuery query;
    query.addQueryItem("per_page", "2");
    query.addQueryItem("query", "created:2022-01-01");
    url.setQuery(query.query());

    QMap<QString, QString> map;
    auto result = GetFromUrl(url, map);
    QJsonDocument jsonDoc(QJsonDocument::fromJson(result));
    QJsonArray array(jsonDoc.array());
    QJsonArray array2;
    qDebug() << array.size();
    for(int i=0; i<array.size(); i++)
    {
        //qDebug() << array[i].isObject();
        QJsonObject item = array[i].toObject();
        //qDebug() << item["title"];
        //qDebug() << item["title"].toString();
        //qDebug() << item["created_at"].toString();
        item.remove("body");
        item.remove("rendered_body");
        item.remove("coediting");
        item.remove("comments_count");
        item.remove("group");
        item.remove("page_views_count");
        item.remove("private");
        item.remove("reactions_count");
        item.remove("team_membership");
        QJsonObject user = item["user"].toObject();
        user.remove("description");
        user.remove("facebook_id");
        user.remove("github_login_name");
        user.remove("linkedin_id");
        user.remove("location");
        user.remove("permanent_id");
        user.remove("team_only");
        user.remove("twitter_screen_name");
        item["user"] = user;
        QJsonArray tags = item["tags"].toArray();
        QJsonArray tags2;
        for(int j=0; j<tags.size(); j++)
        {
            tags2.append(tags[j].toObject()["name"].toString());
        }
        item["tags"] = tags2;
        array2.append(item);
    }
    jsonDoc.setArray(array2);
    qDebug().noquote() << jsonDoc.toJson();
    QString newJson = QString::fromUtf8(jsonDoc.toJson());
    QFile file(qApp->applicationDirPath() + "/json.txt");
    if (file.open(QIODevice::ReadWrite | QIODevice::Text))
    {
        QTextStream out(&file);
#if QT_VERSION >= 0x060000
        out.setEncoding(QStringConverter::Utf8);
#else
        out.setCodec("UTF-8");
#endif
        out << newJson;
        file.close();
    }
    qDebug() << map;
    qDebug() << map["HttpStatus"];
    qDebug() << map["Total-Count"];
    qDebug() << map["Rate-Remaining"];
    qDebug() << map["Rate-Limit"];
    qDebug() << map["Rate-Reset"];
    QVariant rate_reset = map["Rate-Reset"];
    qDebug() << rate_reset.toLongLong();
    QDateTime test_time;
    test_time.setSecsSinceEpoch(rate_reset.toLongLong());
    qDebug()<<test_time.toString("yyyy/MM/dd hh:mm:ss");
}

void Widget::on_btnTest2_clicked()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(qApp->applicationDirPath() + "/qiita.db3");
    db.open();
    QSqlQuery query(db);
    query.exec("create table if not exists Memo(id INTEGER PRIMARY KEY AUTOINCREMENT, Memo text, Created text)");
    //query.exec("delte from Memo");
    query.prepare("insert into Memo (Memo, Created) values (:memo, :created)");
    QString text = "someText";
    query.bindValue(":memo", text);
    QDateTime dt = QDateTime::currentDateTime();
    query.bindValue(":created", dt.toString("yyyy-MM-dd HH:mm:ss.zzz"));
    query.exec();
    query.exec("select * from Memo WHERE Memo = '"+text+"'");
    while (query.next()) {
        int id = query.value("id").toInt();
        QString memo = query.value("Memo").toString();
        qDebug() << QString("id(%1),memo(%2)").arg(id).arg(memo);
    }
    db.close();
}
