#! bash -uvx
set -e
mingwx mk.mgw qdrive
#ucrt64x mk.mgw qdrive
ts=`date "+%Y.%m.%d.%H.%M.%S"`
rm -rf qdrive-*.7z
7z a -t7z qdrive-$ts.7z qdrive-x86_64-static.exe temp -x!temp/*
sha256sum qdrive-$ts.7z
sum1=`sha256sum qdrive-$ts.7z | awk '{print $1}'`
echo $sum1
cat << EOS > qdrive.json
{
    "version": "$ts",
    "description": "",
    "homepage": "",
    "license": "MIT",
    url: [
        "https://gitlab.com/javacommons/qdrive/-/raw/main/qdrive-$ts.7z"
    ],
    "hash": [
        "$sum1"
    ],
    "bin": [
        [
            "qdrive-x86_64-static.exe",
            "qdrive",
            "--dumy"
        ]
    ],
    "shortcuts": [
        [
            "qdrive-x86_64-static.exe",
            "QDrive",
            "--dummy"
        ]
    ],
    "persist": "temp"
}
EOS
git add .
git commit -m.
git push
